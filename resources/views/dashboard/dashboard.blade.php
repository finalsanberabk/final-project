@extends('layout.master')

@section('conten')
<div class="row">
    <div class="col-12">
        <h4>DASHBOARD MANAJEMEN KAS</h4>
    </div>
</div>
<div class="row">
    <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-9">
              <div class="d-flex align-items-center align-self-start">
                <h3 class="mb-0">{{$member}}</h3>
              </div>
            </div>
            <div class="col-3">
              <div class="icon icon-box-success ">
                <span class="mdi mdi-account"></span>
              </div>
            </div>
          </div>
          <h6 class="text-muted font-weight-normal">JUMLAH ANGGOTA</h6>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-9">
              <div class="d-flex align-items-center align-self-start">
                <h3 class="mb-0">Rp {{$kas}}</h3>
              </div>
            </div>
            <div class="col-3">
              <div class="icon icon-box-success">
                <span class="mdi mdi-arrow-top-right icon-item"></span>
              </div>
            </div>
          </div>
          <h6 class="text-muted font-weight-normal">JUMLAH KAS</h6>
        </div>
      </div>
    </div>
    <div class="col-xl-4 col-sm-6 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-9">
              <div class="d-flex align-items-center align-self-start">
                <h3 class="mb-0">Rp {{$pengeluaran}}</h3>
              </div>
            </div>
            <div class="col-3">
              <div class="icon icon-box-danger">
                <span class="mdi mdi-arrow-bottom-left icon-item"></span>
              </div>
            </div>
          </div>
          <h6 class="text-muted font-weight-normal">JUMLAH PENGELUARAN</h6>
        </div>
      </div>
    </div>
  </div>
@endsection
