@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Metode Pembayaran</h4>
            <form class="forms-sample" action="/metode/{{ $metode->id_metode }}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="metode">Nama Metode</label>
                    <input type="text" name="metode" class="form-control" id="metode"
                        value="{{ $metode->nama_metode }}" placeholder="Masukkan metode pembayaran">
                </div>

                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <a href="/metode"><button class="btn btn-dark">Cancel</button></a>
            </form>
        </div>
    </div>
@endsection
