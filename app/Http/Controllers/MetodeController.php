<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Metode;

class MetodeController extends Controller
{

    public function index()
    {
        $metode = Metode::all();
        return view('metode.index', ['metode' => $metode]);
    }


    public function create()
    {
        return view('metode.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'metode' => 'required',
        ]);

        $metode = new Metode;
        $metode->nama_metode = $request->metode;

        $metode->save();
        Alert::success('', 'Tambah Metode Pembayaran berhasil!');

        return redirect('metode');
    }


    public function show($id)
    {
        //
    }


    public function edit($id_metode)
    {
        $metode = Metode::find($id_metode);

        return view('metode.edit', ['metode' => $metode]);
    }


    public function update(Request $request, $id_metode)
    {
        $request->validate([
            'metode' => 'required',
        ]);

        $metode = Metode::find($id_metode);
        $metode->nama_metode = $request->metode;

        $metode->save();
        Alert::success('', 'Metode Pembayaran berhasil diedit!');

        return redirect('metode');
    }


    public function destroy($id_metode)
    {
        $metode = Metode::find($id_metode);
        $metode->delete();

        Alert::success('', 'Metode berhasil dihapus!');
        return redirect('/metode');
    }
}
