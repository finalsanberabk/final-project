@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Daftar Setoran Kas</h4>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Jumlah Kas</th>
                            <th>Tanggal Pembayaran</th>
                            <th>Metode Pembayaran</th>
                            <th>Nama Anggota</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($kas as $key => $kas)
                            <tr>
                                <td>{{ $kas['jumlah'] }}</td>
                                <td>{{ $kas['tanggal'] }}</td>



                                @forelse ($metode as $item)
                                    @if ($item->id_metode === $kas->id_metode)
                                        <td>{{ $item->nama_metode }}</td>
                                    @else
                                        <td>No items</td>
                                    @endif
                                @empty
                                @endforelse

                                @forelse ($members as $item)
                                    @if ($item->id_anggota === $kas->id_anggota)
                                        <td>{{ $item->nama }}</td>
                                    @else
                                    @endif
                                @empty
                                @endforelse


                                <td>
                                    <div class="d-flex">
                                        <a href='/kas/{{ $kas['id_kas'] }}/edit' type="button"
                                            class="btn btn-dark btn-icon-text mx-1"> Edit
                                        </a>
                                        <form action="/kas/{{ $kas['id_kas'] }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon-text"> Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada kas yang masuk</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
