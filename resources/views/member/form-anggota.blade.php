@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Anggota</h4>
            <p class="card-description"> Tambah Anggota Manajemen Kas </p>
            <form class="forms-sample" action="/member" method="post">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{ old('nama', '')}}" placeholder="Ketikkan nama">
                </div>
                <div class="form-group">
                    <label for="jenis-kelamin">Jenis Kelamin</label>
                    <select class="form-control" name="jenis-kelamin" id="jenis-kelamin">
                        <option value='laki-laki'>Laki-Laki</option>
                        <option value='perempuan'>Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value='aktif'>Aktif</option>
                        <option value='nonaktif'>Non-Aktif</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-dark">Cancel</button>
            </form>
        </div>
    </div>
@endsection
