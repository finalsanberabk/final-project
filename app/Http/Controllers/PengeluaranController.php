<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Pengeluaran;
use App\Admin;



class PengeluaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengeluaran = Pengeluaran::all();
        return view('pengeluaran.index', ['pengeluaran' => $pengeluaran]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admin = Admin::all();

        return view('pengeluaran.create', ['admin' => $admin]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_pengeluaran' => 'required',
            'jumlah_pengeluaran' => 'required',
            'tanggal' => 'required',
            'id_admin' => 'required',
        ]);
        $pengeluaran = new Pengeluaran;
        $pengeluaran->nama_pengeluaran = $request->nama_pengeluaran;
        $pengeluaran->jumlah_pengeluaran = $request->jumlah_pengeluaran;
        $pengeluaran->tanggal = $request->tanggal;
        $pengeluaran->id_admin = $request->id_admin;

        $pengeluaran->save();
        Alert::success('', 'Tambah data pengeluaran berhasil!');

        return redirect('/pengeluaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengeluaran = Pengeluaran::find($id);

        return view('pengeluaran.detail', ['pengeluaran' => $pengeluaran]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengeluaran = Pengeluaran::find($id);
        $admin = Admin::all();
        return view('pengeluaran.edit', ['pengeluaran' => $pengeluaran, 'admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_pengeluaran' => 'required',
            'jumlah_pengeluaran' => 'required',
            'tanggal' => 'required',
            'id_admin' => 'required'
        ]);
        $pengeluaran = Pengeluaran::find($id);

        $pengeluaran->nama_pengeluaran = $request->nama_pengeluaran;
        $pengeluaran->jumlah_pengeluaran = $request->jumlah_pengeluaran;
        $pengeluaran->tanggal = $request->tanggal;
        $pengeluaran->id_admin = $request->id_admin;

        $pengeluaran->save();
        Alert::success('', 'Ubah catatan pengeluaran berhasil!');
        return redirect('/pengeluaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_pengeluaran)
    {
        $pengeluaran = Pengeluaran::find($id_pengeluaran);
        $pengeluaran->delete();

        Alert::success('', 'Catatan pengeluaran berhasil dihapus!');
        return redirect('pengeluaran');
    }
}
