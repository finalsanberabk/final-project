@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Metode Pembayaran</h4>
            <form class="forms-sample" action="/metode" method="post">
                @csrf
                <div class="form-group">
                    <label for="metode">Nama Metode</label>
                    <input type="text" name="metode" class="form-control" id="metode"
                        placeholder="Masukkan metode pembayaran">
                </div>

                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-dark">Cancel</button>
            </form>
        </div>
    </div>
@endsection
