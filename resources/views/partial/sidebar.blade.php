<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
        <a class="sidebar-brand brand-logo text-decoration-none text-light" href="../../index.html">Cash App</a>
        <a class="sidebar-brand brand-logo-mini text-decoration-none text-light" href="../../index.html">C</a>
    </div>
    <ul class="nav">
        <li class="nav-item profile">
            <div class="profile-desc">
                <div class="profile-pic">
                    <div class="count-indicator">
                        <img class="img-xs rounded-circle "
                            src="{{ asset('coronaAdmin/template/assets/images/faces/face15.jpg') }}" alt="">
                        <span class="count bg-success"></span>
                    </div>
                    <div class="profile-name">
                        <h5 class="mb-0 font-weight-normal">
                            @if (Auth::check())
                                {{ Auth::user()->nama }}
                            @endif
                        </h5>
                        <span>Gold Member</span>
                    </div>
                </div>
                <a href="#" id="profile-dropdown" data-toggle="dropdown"><i class="mdi mdi-dots-vertical"></i></a>
                <div class="dropdown-menu dropdown-menu-right sidebar-dropdown preview-list"
                    aria-labelledby="profile-dropdown">
                    <a href="#" class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-dark rounded-circle">
                                <i class="mdi mdi-settings text-primary"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <p class="preview-subject ellipsis mb-1 text-small">Account settings</p>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item preview-item">
                        <div class="preview-thumbnail">
                            <div class="preview-icon bg-dark rounded-circle">
                                <i class="mdi mdi-onepassword  text-info"></i>
                            </div>
                        </div>
                        <div class="preview-item-content">
                            <p class="preview-subject ellipsis mb-1 text-small">Change Password</p>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                </div>
            </div>
        </li>
        <li class="nav-item nav-category">
            <span class="nav-link">Navigation</span>
        </li>
        <li class="nav-item menu-items">
            <a class="nav-link" href="/home">
                <span class="menu-icon">
                    <i class="mdi mdi-speedometer"></i>
                </span>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item menu-items mb-2 mt-4">
            <a class="nav-link" data-toggle="collapse" href="#anggota" aria-expanded="false" aria-controls="anggota">
                <span>
                    <i></i>
                </span>
                <span class="menu-title">Manajemen Anggota</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="anggota">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="../../member/create">Tambah Anggota</a></li>
                    <li class="nav-item"> <a class="nav-link" href="../../member">Daftar Anggota</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item menu-items mb-2">
            <a class="nav-link" data-toggle="collapse" href="#keuangan" aria-expanded="false" aria-controls="keuangan">
                <span>
                    <i></i>
                </span>
                <span class="menu-title">Manajemen Keuangan</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="keuangan">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="../../kas/create"> Setor Kas </a></li>
                    <li class="nav-item"> <a class="nav-link" href="../../kas"> Daftar Kas </a></li>
                    <li class="nav-item"> <a class="nav-link" href="../../pengeluaran/create"> Catatan Pengeluaran </a>
                    </li>
                    <li class="nav-item"> <a class="nav-link" href="../../pengeluaran"> Daftar Pengeluaran </a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item menu-items">
            <a class="nav-link" data-toggle="collapse" href="#pembayaran" aria-expanded="false"
                aria-controls="pembayaran">
                <span>
                    <i></i>
                </span>
                <span class="menu-title">Manajemen Pembayaran</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="pembayaran">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="../../metode/create"> Tambah Metode</a></li>
                    <li class="nav-item"> <a class="nav-link" href="../../metode"> List Metode</a></li>
                </ul>
            </div>
        </li>

    </ul>
</nav>
