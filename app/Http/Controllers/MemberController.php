<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Profile;
use RealRashid\SweetAlert\Facades\Alert;


class MemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('member.daftar-anggota', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('member.form-anggota');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $validatedData = $request->validate([
            'nama' => 'required',
            'jenis-kelamin' => 'required',
            'status' => 'required'
        ]);

        $member = Member::create([
            'nama' => $request['nama'],
            'jenis_kelamin' => $request['jenis-kelamin'],
            'status' => $request['status']
        ]);

        Alert::success('', 'Tambah anggota Manajemen Kas berhasil!');

        return redirect('member');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $member = Member::with('profile')->where('id_anggota', $id)->first();
        return view('member.anggota', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $member = Member::with('profile')->where('id_anggota', $id)->first();
        return view('member.profile-anggota', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
        $member = Member::find($id);

        $member['nama'] = $request['nama'];
        $member['jenis_kelamin'] = $request['jenis-kelamin'];
        $member['status'] = $request['status'];

        $member->save();

        $member->profile()->updateOrCreate(
            ['id_anggota' => $id],
            [
                'tanggal_lahir' => $request['tanggal-lahir'],
                'bio' => $request['bio'],
                'alamat' => $request['alamat'],
                'jurusan' => $request['jurusan'],
                'semester' => $request['semester']
            ]
        );
        Alert::success('', 'Perbarui anggota Manajemen Kas berhasil!');
        return redirect('member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $deleted = Member::where('id_anggota', $id)->delete();
        return redirect('member');
    }
}
