<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    //
    protected $table = 'profile';
    protected $primaryKey = 'id_profile';
    protected $guarded = [];

    // public function members()
    // {
    //     return $this->belongsTo(Member::class);
    // }
}
