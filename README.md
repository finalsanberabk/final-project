## Final Project

## Kelompok 6

## Anggota Kelompok

-   Bima Hamdani Mawaridi
-   Arda Amanda
-   Kamila Khalishah

## Tema Project

Manajemen Kas

## ERD

![](./public/erd/erd.png)

## Link Video

Link demo aplikasi : https://youtu.be/RnZhlehNsM8
Link deploy : http://finalproject.manajemenkasapp.sanbercodeapp.com
