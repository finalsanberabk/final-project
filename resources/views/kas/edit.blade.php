@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Edit daftar Kas</h4>
            <form class="forms-sample" action="/kas/{{ $kas->id_kas }}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="jumlah">Jumlah Kas</label>
                    <input type="number" name="jumlah" class="form-control" id="jumlah" value="{{ $kas->jumlah }}"
                        placeholder="Masukkan jumlah uang kas">
                </div>
                <div class="form-group">
                    <label for="tanggal">Tanggal Pembayaran</label>
                    <input type="date" value="{{ $kas->tanggal }}" name="tanggal" class="form-control" id="tanggal">
                </div>
                <div class="form-group">
                    <label for="id_metode">Metode Pembayaran</label>
                    <select class="form-control" name="id_metode" id="id_metode">
                        <option value=''>---Pilih Metode---</option>

                        @forelse ($metode as $item)
                            @if ($item->id_metode === $kas->id_metode)
                                <option value='{{ $item->id_metode }}' selected>{{ $item->nama_metode }}</option>
                            @else
                                <option value='{{ $item->id_metode }}'>{{ $item->nama_metode }}</option>
                            @endif

                        @empty
                            <option value=''>Tidak ada metode</option>
                        @endforelse

                    </select>
                </div>
                <div class="form-group">
                    <label for="id_anggota">Nama Anggota</label>
                    <select class="form-control" name="id_anggota" id="id_anggota">
                        <option value=''>---Pilih Anggota---</option>

                        @forelse ($members as $item)
                            @if ($item->id_anggota === $kas->id_anggota)
                                <option value='{{ $item->id_anggota }}' selected>{{ $item->nama }}</option>
                            @else
                                <option value='{{ $item->id_anggota }}'>{{ $item->nama }}</option>
                            @endif

                        @empty
                            <option value=''>Tidak ada anggota</option>
                        @endforelse

                    </select>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-dark">Cancel</button>
            </form>
        </div>
    </div>
@endsection
