<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Kas;
use App\Metode;
use App\Member;

class KasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kas = Kas::all();
        $metode = Metode::all();
        $members = Member::all();
        return view('kas.index', ['kas' => $kas, 'members' => $members, 'metode' => $metode]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metode = Metode::all();
        $members = Member::all();
        return view('kas.create', ['members' => $members], ['metode' => $metode]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jumlah' => 'required',
            'tanggal' => 'required',
            'id_metode' => 'required',
            'id_anggota' => 'required'
        ]);


        $kas = new Kas;

        $kas->jumlah = $request->jumlah;
        $kas->tanggal = $request->tanggal;
        $kas->id_metode = $request->id_metode;
        $kas->id_anggota = $request->id_anggota;

        $kas->save();
        Alert::success('', 'Tambah Setoran Kas berhasil!');

        return redirect('kas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_kas)
    {
        $kas = Kas::find($id_kas);
        $metode = Metode::all();
        $members = Member::all();

        return view('kas.edit', ['kas' => $kas, 'members' => $members, 'metode' => $metode]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_kas)
    {
        $request->validate([
            'jumlah' => 'required',
            'tanggal' => 'required',
            'id_metode' => 'required',
            'id_anggota' => 'required'
        ]);

        $kas = Kas::find($id_kas);
        $kas->jumlah = $request->jumlah;
        $kas->tanggal = $request->tanggal;
        $kas->id_metode = $request->id_metode;
        $kas->id_anggota = $request->id_anggota;

        $kas->save();
        Alert::success('', 'Tambah Setoran Kas berhasil!');

        return redirect('kas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_kas)
    {
        $kas = Kas::find($id_kas);
        $kas->delete();
        return redirect('/kas');
    }
}
