@extends('layout.master')


@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Edit Pengeluaran</h4>
            <p class="card-description"> Tambah Pengeluaran </p>
            <form action="/pengeluaran/{{ $pengeluaran->id_pengeluaran }}" method="POST">
                @csrf
                @method('put')
                <div class="form-group">
                    <label>Pengeluaran:</label>
                    <input name="nama_pengeluaran" value="{{ $pengeluaran->nama_pengeluaran }}" type="text"
                        class="form-control">

                </div>
                @error('nama_pengeluaran')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Jumlah Pengeluaran:</label>
                    <input name="jumlah_pengeluaran" value="{{ $pengeluaran->jumlah_pengeluaran }}" type="number"
                        class="form-control">
                </div>
                @error('jumlah_pengeluaran')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>tanggal</label>
                    <input name="tanggal" value="{{ $pengeluaran->tanggal }}" type="date" class="form-control">
                </div>
                @error('tanggal')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="id_admin">Admin</label>
                    <select class="form-control" name="id_admin" id="id_admin">
                        <option value=''>---Pilih Admin---</option>
                        @forelse ($admin as $item)
                            @if ($item->id_admin === $pengeluaran->id_admin)
                                <option value='{{ $item->id_admin }}' selected>{{ $item->nama }}</option>
                            @else
                                <option value='{{ $item->id_admin }}'>{{ $item->nama }}</option>
                            @endif
                        @empty
                            <option value=''>Tidak ada admin</option>
                        @endforelse
                    </select>
                </div>
                @error('id_admin')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
