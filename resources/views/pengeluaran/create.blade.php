@extends('layout.master')


@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Pengeluaran</h4>
            <p class="card-description"> Tambah Pengeluaran </p>
            <form action="/pengeluaran" method="POST">
                @csrf
                <div class="form-group">
                    <label>Pengeluaran:</label>
                    <input name="nama_pengeluaran" type="text" class="form-control">
                </div>
                @error('nama_pengeluaran')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>Jumlah Pengeluaran:</label>
                    <input name="jumlah_pengeluaran" type="number" class="form-control">
                </div>
                @error('jumlah_pengeluaran')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label>tanggal</label>
                    <input name="tanggal" type="date" class="form-control">
                </div>
                @error('tanggal')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="id_admin">Admin</label>
                    <select class="form-control" name="id_admin" id="id_admin">
                        <option value=''>---Pilih Admin---</option>
                        @forelse ($admin as $item)
                            <option value='{{ $item->id_admin }}'>{{ $item->nama }}</option>
                        @empty
                            <option value=''>Tidak ada admin</option>
                        @endforelse
                    </select>
                </div>
                @error('id_admin')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
