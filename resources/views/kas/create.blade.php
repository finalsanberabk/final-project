@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Kas</h4>
            <form class="forms-sample" action="/kas" method="post">
                @csrf
                <div class="form-group">
                    <label for="jumlah">Jumlah Kas</label>
                    <input type="number" name="jumlah" class="form-control" id="jumlah"
                        placeholder="Masukkan jumlah uang kas">
                </div>
                <div class="form-group">
                    <label for="tanggal">Tanggal Pembayaran</label>
                    <input type="date" name="tanggal" class="form-control" id="tanggal">
                </div>
                <div class="form-group">
                    <label for="id_metode">Metode Pembayaran</label>
                    <select class="form-control" name="id_metode" id="id_metode">
                        <option value=''>---Pilih Metode---</option>

                        @forelse ($metode as $item)
                            <option value='{{ $item->id_metode }}'>{{ $item->nama_metode }}</option>
                        @empty
                            <option value=''>Tidak ada metode</option>
                        @endforelse

                    </select>
                </div>
                <div class="form-group">
                    <label for="id_anggota">Nama Anggota</label>
                    <select class="form-control" name="id_anggota" id="id_anggota">
                        <option value=''>---Pilih Anggota---</option>

                        @forelse ($members as $item)
                            <option value='{{ $item->id_anggota }}'>{{ $item->nama }}</option>
                        @empty
                            <option value=''>Tidak ada anggota</option>
                        @endforelse

                    </select>
                </div>
                <button type="submit" class="btn btn-primary mr-2">Submit</button>
                <button class="btn btn-dark">Cancel</button>
            </form>
        </div>
    </div>
@endsection
