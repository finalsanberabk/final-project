@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Anggota Manajamen Kas</h4>
            <form class="forms-sample" action="/member/{{ $member['id_anggota'] }}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control" id="nama" value='{{ $member['nama'] }}'>
                </div>
                <div class="form-group">
                    <label for="jenis-kelamin">Jenis Kelamin</label>
                    <select class="form-control" name="jenis-kelamin" id="jenis-kelamin">
                        <option value='laki-laki' @if ($member['jenis_kelamin'] == 'laki-laki') selected @endif>Laki-Laki</option>
                        <option value='perempuan' @if ($member['jenis_kelamin'] == 'perempuan') selected @endif>Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value='aktif' @if ($member['status'] == 'aktif') selected @endif>Aktif</option>
                        <option value='nonaktif' @if ($member['status'] == 'nonaktif') selected @endif>Non-Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tanggal-lahir">Tanggal Lahir</label>
                    <input type="date" name="tanggal-lahir" class="form-control" id="tanggal-lahir"
                        @if (!is_null($member->profile)) value='{{ $member->profile['tanggal_lahir'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" name="bio" class="form-control" id="bio"
                        @if (!is_null($member->profile)) value='{{ $member->profile['bio'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat"
                        @if (!is_null($member->profile)) value='{{ $member->profile['alamat'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" name="jurusan" class="form-control" id="jurusan"
                        @if (!is_null($member->profile)) value='{{ $member->profile['jurusan'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="semester">Semester</label>
                    <input type="text" name="semester" class="form-control" id="semester"
                        @if (!is_null($member->profile)) value='{{ $member->profile['semester'] }}' @endif>
                </div>
                <button type='submit' class="btn btn-primary">Save</button>
                <a href="/member" class="btn btn-light">Back</a>
        </div>
    </div>
@endsection
