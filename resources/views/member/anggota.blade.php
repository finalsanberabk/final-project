@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Anggota Manajamen Kas</h4>
            <form class="forms-sample">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama" class="form-control" id="nama" disabled
                        placeholder='{{ $member['nama'] }}'>
                </div>
                <div class="form-group">
                    <label for="jenis-kelamin">Jenis Kelamin</label>
                    <select class="form-control" name="jenis-kelamin" id="jenis-kelamin" disabled>
                        <option value='laki-laki' @if ($member['jenis_kelamin'] == 'laki-laki') selected @endif>Laki-Laki</option>
                        <option value='perempuan' @if ($member['jenis_kelamin'] == 'perempuan') selected @endif>Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" id="status" disabled>
                        <option value='aktif' @if ($member['status'] == 'aktif') selected @endif>Aktif</option>
                        <option value='nonaktif' @if ($member['status'] == 'nonaktif') selected @endif>Non-Aktif</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tanggal-lahir">Tanggal Lahir</label>
                    <input type="text" name="tanggal-lahir" class="form-control" id="tanggal-lahir" disabled
                        @if (!is_null($member->profile)) placeholder='{{ $member->profile['tanggal_lahir'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" name="bio" class="form-control" id="bio" disabled
                        @if (!is_null($member->profile)) placeholder='{{ $member->profile['bio'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="alamat">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="alamat" disabled
                        @if (!is_null($member->profile)) placeholder='{{ $member->profile['alamat'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="jurusan">Jurusan</label>
                    <input type="text" name="jurusan" class="form-control" id="jurusan" disabled
                        @if (!is_null($member->profile)) placeholder='{{ $member->profile['jurusan'] }}' @endif>
                </div>
                <div class="form-group">
                    <label for="semester">Semester</label>
                    <input type="text" name="semester" class="form-control" id="semester" disabled
                        @if (!is_null($member->profile)) placeholder='{{ $member->profile['semester'] }}' @endif>
                </div>
                <a href="/member" class="btn btn-dark">Back</a>
        </div>
    </div>
@endsection
