@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Daftar Anggota</h4>
            <p class="card-description"> Daftar Anggota Manajemen Kas</code>
            </p>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($members as $key => $member)
                            <tr>
                                <td>{{ $member['nama'] }}</td>
                                <td>{{ $member['jenis_kelamin'] }}</td>
                                <td><label
                                        @if ($member['status'] == 'aktif') class="badge badge-success" 
                                @else
                                    class="badge badge-danger" @endif>{{ $member['status'] }}</label>
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <a href='/member/{{ $member['id_anggota'] }}/edit' type="button"
                                            class="btn btn-dark btn-icon-text mx-1"> Edit
                                        </a>
                                        <form action="/member/{{ $member['id_anggota'] }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon-text"> Delete
                                            </button>
                                        </form>
                                        <a href='/member/{{ $member['id_anggota'] }}' type="button"
                                            class="btn btn-secondary btn-icon-text mx-1"> View </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">No users</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
