@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Daftar Metode Pembayaran</h4>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Metode Pembayaran</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($metode as $key => $metode)
                            <tr>

                                <td>{{ $key + 1 }}</td>
                                <td>{{ $metode['nama_metode'] }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href='/metode/{{ $metode['id_metode'] }}/edit' type="button"
                                            class="btn btn-dark btn-icon-text mx-1"> Edit
                                        </a>
                                        <form action="/metode/{{ $metode['id_metode'] }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon-text"> Delete
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">Tidak ada metode yang masuk</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
