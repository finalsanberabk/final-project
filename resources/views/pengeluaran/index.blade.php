@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Pengeluaran</h4>
            <p class="card-description"> Daftar Pengeluaran</code>
            </p>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Pengeluaran</th>
                            <th>Jumlah pengeluaran</th>
                            <th>tanggal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($pengeluaran as $key => $pengeluaran)
                            <tr>
                                <td>{{ $pengeluaran['nama_pengeluaran'] }}</td>
                                <td>{{ $pengeluaran['jumlah_pengeluaran'] }}</td>
                                <td>{{ $pengeluaran['tanggal'] }}
                                </td>
                                <td>
                                    <div class="d-flex">
                                        <form action="/pengeluaran/{{ $pengeluaran['id_pengeluaran'] }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-icon-text"> Delete
                                            </button>
                                        </form>
                                        <a href="/pengeluaran/{{ $pengeluaran['id_pengeluaran'] }}"
                                            class="btn btn-info btn-sm mx-3">
                                            Detail</a>
                                        <a href="/pengeluaran/{{ $pengeluaran['id_pengeluaran'] }}/edit"
                                            class="btn btn-warning btn-sm">
                                            Edit</a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">No users</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
