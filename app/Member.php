<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    protected $primaryKey = 'id_anggota';
    protected $table = 'anggota';
    protected $guarded = [];

    public function profile()
    {
        return $this->hasOne(Profile::class, 'id_anggota');
    }
}
