@extends('layout.master')

@section('conten')
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Detail Pengeluaran</h4>
            <form class="forms-sample" action="/pengeluaran/{{ $pengeluaran['id_anggota'] }}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="nama">Pengeluaran</label>
                    <input type="text" name="nama_pengeluaran" class="form-control text-dark" id="nama"
                        value='{{ $pengeluaran['nama_pengeluaran'] }}' disabled>
                </div>
                <div class="form-group">
                    <label for="jenis-kelamin">Jumlah Pengeluaran</label>
                    <input type="text" name="jumlah_pengeluaran" class="form-control text-dark" id="nama"
                        value='{{ $pengeluaran['jumlah_pengeluaran'] }}' disabled>
                </div>
                <div class="form-group">
                    <label for="status">Tanggal</label>
                    <input type="text" name="tanggal" class="form-control text-dark" id="nama"
                        value='{{ $pengeluaran['tanggal'] }}' disabled>
                </div>

                <a href="/pengeluaran" class="btn btn-light">Back</a>
        </div>
    </div>
@endsection
